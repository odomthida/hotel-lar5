$(document).ready(function(){
    console.log("admin page js");
    $userProfile = $(".user-profile");

    if ($userProfile.length) {
        console.log("userProfile");

        $edit = $(".user-profile .edit");
        $save = $(".user-profile .save");
        $cancel = $(".user-profile .cancel");
        $upload = $(".user-profile .upload");

        $edit.click(function() {
            $userProfile.find('.input').removeAttr("disabled").removeClass("disabled");
            $userProfile.find(".input[name=dob]").focus();
            $edit.addClass("disabled");
            $save.removeClass("disabled");
            $cancel.removeClass("disabled");
        });

        $upload.click(function () {
            console.log("Upload");
        });

        $cancel.click(function () {
            $save.addClass("disabled");
            $cancel.addClass("disabled");
            $edit.removeClass("disabled");
            $userProfile.find('.input').attr("disabled","").addClass("disabled");
        });

        $save.click(function() {
            $.ajax({
                'url': '/user/update-user',
                'method':'POST',
                'data': {
                    'dob': $('.input[name="dob"]').val(),
                    'gender': $('.input[name="gender"]').val(),
                    'phone': $('.input[name="phone"]').val(),
                    'address': $('textarea[name="address"]').val()

                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }


            }).done(function(){
                console.log("done");
                $cancel.trigger('click');
            }).success(function(data){
                console.log(data);
            }).fail(function(data){
                console.log(data);
            });

        });
    }

    $datepicker = $('.datepicker');
    if ($datepicker.length) {
        $datepicker.datepicker();
    }

    $confirm = $('.confirm');
    if ($confirm.length) {

        $confirm.confirm({
            text: "Are you sure you want to delete?",
            title: "Confirmation required",
            confirm: function(button) {
                $.ajax( {
                        'url': button.data('action'),
                        'method':'POST',
                        'data': {
                            'id': button.data('record')
                        },
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    }
                ).success(function(data){
                        location.reload();
                    });
            },
            cancel: function(button) {
                // nothing to do
            },
            confirmButton: "Yes, <i class='fa fa-trash'></i>",
            cancelButton: "No",
            post: true,
            confirmButtonClass: "btn-danger",
            cancelButtonClass: "btn-default",
            dialogClass: "modal-dialog modal-lg" // Bootstrap classes for large modal

        });
    }

    $fileUpload = $("#fileupload");
    if ($fileUpload.length) {
        $btnFileupload =$("#btnFileUpload");
        $btnFileupload.click(function(){
            $fileUpload.trigger('click');
        });
        $fileUpload.change(function(){
            console.log("change");
            $("form").submit();
        });
    }

    $deleteImageBtn = $('#deleteImageBtn');
    if ($deleteImageBtn.length) {
        var cookie = $.cookie('isHidden');
        $removeIcon =$('.confirm')
        $deleteImageBtn.click(function(){
            $.cookie('isHidden', 0, {path: '/'});
            $removeIcon.removeClass("hidden");
            $("#cancelImage").removeClass("hidden");
        });

        $('#cancelImage').click(function() {
            $removeIcon.addClass("hidden");
            $(this).addClass('hidden  ');
            $.cookie('isHidden', 1, {path: '/'});
        });

        if ( cookie == "1" ) {
            console.log(cookie);
            $('#cancelImage').trigger('click');
        }


        $roomPicture = $('.room-picture .col-sm-2 a');
        $roomPicture.magnificPopup({
            type: 'image',
            gallery: {
                enabled:true
            },
            mainClass: 'mfp-with-zoom', // this class is for CSS animation below

            zoom: {
                enabled: true, // By default it's false, so don't forget to enable it

                duration: 300, // duration of the effect, in milliseconds
                easing: 'ease-in-out', // CSS transition easing function

                // The "opener" function should return the element from which popup will be zoomed in
                // and to which popup will be scaled down
                // By defailt it looks for an image tag:
                opener: function(openerElement) {
                    // openerElement is the element on which popup was initialized, in this case its <a> tag
                    // you don't need to add "opener" option if this code matches your needs, it's defailt one.
                    return openerElement.is('img') ? openerElement : openerElement.find('img');
                }
            }
        });



        $itemRoom = $('.room-picture a img');
        var defaultH = $($itemRoom[0]).height();
        console.log(defaultH);
        $itemRoom.each(function(key,value){
            $(value).css("height", defaultH);
        });



    }

    $gallery = $('a.gallery');
    if ($gallery.length) {
        $gallery.click(function() {
            $.cookie('isHidden', 1, {path: '/'});
        });
    }


    $table = $('.datatable');
    if ($table.length) {
        $table.dataTable();
    }

    if ($('#photogallery').length) {
        console.log('test');
        $('#photogallery .col-sm-2 a').magnificPopup({
            type: 'image',
            gallery: {
                enabled:true
            },
            mainClass: 'mfp-with-zoom', // this class is for CSS animation below

            zoom: {
                enabled: true, // By default it's false, so don't forget to enable it

                duration: 300, // duration of the effect, in milliseconds
                easing: 'ease-in-out', // CSS transition easing function

                // The "opener" function should return the element from which popup will be zoomed in
                // and to which popup will be scaled down
                // By defailt it looks for an image tag:
                opener: function(openerElement) {
                    // openerElement is the element on which popup was initialized, in this case its <a> tag
                    // you don't need to add "opener" option if this code matches your needs, it's defailt one.
                    return openerElement.is('img') ? openerElement : openerElement.find('img');
                }
            }
        });
    }

});