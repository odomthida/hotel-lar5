/**
 * Created by thida on 3/14/15.
 */

$(document).ready(function () {
    $(".delete").click(function () {
        if (!confirm("Do you want to delete")) {
            return false;
        }
    });

    jQuery(window).load(function () {
        "use strict";
        jQuery('.slider, .slideshow-shortcode').flexslider({
            animation: "fade",
            controlNav: false,
            directionNav: true,
            slideshow: true,
            start: function (slider) {
                jQuery('body').removeClass('loading');
            },
            prevText: "",
            nextText: "",
            smoothHeight: true
        });
        jQuery('.text-slider').flexslider({
            animation: "fade",
            controlNav: false,
            directionNav: true,
            slideshow: true,
            start: function (slider) {
                jQuery('body').removeClass('loading');
            },
            prevText: "",
            nextText: ""
        });
    });

    $(function() {
        $( ".datepicker" ).datepicker({
	        numberOfMonths: 2,
	        showButtonPanel: true
        });
    });
    console.log('room');

    $('.magnificPopup a').magnificPopup(
        {

            type: 'image',
            gallery: {
                enabled:true
            },
            mainClass: 'mfp-with-zoom', // this class is for CSS animation below

            zoom: {
                enabled: true, // By default it's false, so don't forget to enable it

                duration: 300, // duration of the effect, in milliseconds
                easing: 'ease-in-out', // CSS transition easing function

                // The "opener" function should return the element from which popup will be zoomed in
                // and to which popup will be scaled down
                // By defailt it looks for an image tag:
                opener: function(openerElement) {
                    // openerElement is the element on which popup was initialized, in this case its <a> tag
                    // you don't need to add "opener" option if this code matches your needs, it's defailt one.
                    return openerElement.is('img') ? openerElement : openerElement.find('img');
                }
            }
        }
    );
});

function showPassword() {

    var key_attr = $('#key').attr('type');

    if(key_attr != 'text') {

        $('.checkbox').addClass('show');
        $('#key').attr('type', 'text');

    } else {

        $('.checkbox').removeClass('show');
        $('#key').attr('type', 'password');
    }
}



