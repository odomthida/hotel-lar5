@extends('layout.admin.main')

{{--Section Content--}}
@section("content")
    <!-- Page Content -->
    <div class="row roomlist">
        <div class="col-lg-6">
            <h2> Edit user form</h2>
            <hr/>
            {!! Form::open(array('url' => 'admin/edit-user/'.$user->id, 'method' => 'POST')) !!}
            <div class="form-group">
                {!! Form::label('username', 'Username' , array('class' => '')) !!}
                {!! Form::text('username', $user->username, array('class' => 'form-control', 'placeholder'=>"Username")) !!}
            </div>

            <div class="form-group">
                {!! Form::label('email', 'Email' , array('class' => '')) !!}
                {!! Form::email('email', $user->email, array("class"=>"form-control", 'placeholder' => 'Email', 'required'=>"")); !!}
            </div>

            <div class="form-group">
                {!! Form::label('usertype', 'Role' , array('class' => '')) !!}
                {!! Form::select('usertype', $user_types  , $user->usertype->id, array('class' => 'form-control')) !!}
            </div>
            <div class="form-group">
                <div class="checkbox">
                    <label>
                        {!! Form::radio('active', '1', 1==$user->active, array('class' => 'name')) !!}
                        Active
                    </label>
                </div>
                <div class="checkbox">
                    <label>
                        {!! Form::radio('active', '0', 0==$user->active, array('class' => 'name')) !!}
                        Disable
                    </label>
                </div>
            </div>
            {!! Form::submit('Submit!', array('class' => 'btn btn-primary')) !!}
            <a href="/admin/list-user" class="btn btn-primary"><i class="fa fa-th-list"></i> List</a>
            {!! Form::close()!!}

        </div>
    </div>
@stop