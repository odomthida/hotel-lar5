@extends('layout.admin.main')

{{--Section Content--}}
@section("content")
    <!-- Page Content -->
    <div class="row roomlist">
        <div class="col-lg-10">
            <h1>People Reservation </h1>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>No</th>
                    <th>Name</th>
                    <th>Type</th>
                    <th>Description</th>
                    <th>Other</th>
                </tr>
                </thead>
                <tbody>
                @for($i=0; $i<4; $i++ )
                    <tr>
                        <td>{{$i + 1}}</td>
                        <td> 3</td>
                        <td>3333</td>
                        <td>24$</td>
                        <td>
                            <a href="#edit"><i class="fa fa-edit fa-2x"></i></a>
                            <a href="#gallery_view"><i class="fa fa-picture-o fa-2x"></i></a>
                            <a href="#disabled" class="disabled" title="Hide Item"><i class="fa fa-trash-o text-danger fa-2x"></i></a>

                        </td>
                    </tr>

                @endfor

                </tbody>
            </table>
        </div>
    </div>
@stop