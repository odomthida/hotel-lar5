@extends('layout.admin.main')

{{--Section Content--}}
@section("content")
    <!-- Page Content -->
    <div class="row roomlist">
        <div class="col-lg-6">
            <h2> Create user form</h2>
            <hr/>
            {!! Form::open(array('url' => 'admin/add-user', 'method' => 'POST')) !!}
            <div class="form-group">
                {!! Form::label('username', 'Username' , array('class' => '')) !!}
                {!! Form::text('username', "", array('class' => 'form-control', 'placeholder'=>"Username")) !!}
            </div>

            <div class="form-group">
                {!! Form::label('password', 'Password' , array('class' => '')) !!}
                {!! Form::password('password', array("class"=>"form-control", 'placeholder'=>"Password", 'required'=>"")) !!}
            </div>

            <div class="form-group">
                {!! Form::label('email', 'Email' , array('class' => '')) !!}
                {!! Form::email('email', "", array("class"=>"form-control", 'placeholder' => 'Email', 'required'=>"")); !!}
            </div>

            <div class="form-group">
                {!! Form::label('usertype', 'Role' , array('class' => '')) !!}
                {!! Form::select('usertype', $user_types  , '', array('class' => 'form-control')) !!}
            </div>
            {!! Form::submit('Submit!', array('class' => 'btn btn-primary')) !!}

            {!! Form::close()!!}
        </div>
    </div>
@stop