@extends('layout.admin.main')

{{--Section Content--}}
@section("content")
    <!-- Page Content -->
    <div class="row roomlist">
        <div class="col-lg-10">
            <h1>List Users </h1>
            <a href="/admin/add-user" class="add-room btn btn-danger"><i class="fa fa-plus"></i> Create new user </a>
            <table class="table table-bordered datatable">
                <thead>
                <tr>
                    <th>No</th>
                    <th>Name</th>
                    <th>Role</th>
                    <th>Email</th>
                    <th>status</th>
                    <th>Other</th>
                </tr>
                </thead>
                <tbody>
                @for($i=0; $i<count($users); $i++ )
                    <tr>
                        <td>{{$i + 1}}</td>
                        <td> {{ $users[$i]->username }}</td>
                        <td> {{$users[$i]->usertype->toArray()['name']}}</td>
                        <td> {{$users[$i]->email}}</td>
                        <td> @if ( $users[$i]->active)
                                 Active
                            @else
                                 Disable
                            @endif
                            </td>
                        <td>
                            <a href="/admin/edit-user/{{$users[$i]->id}}"><i class="fa fa-edit fa-2x"></i></a>
                            <a href="#disabled" class="confirm" data-action="/admin/delete-user" data-record="{{$users[$i]->id}}" title="Hide Item"><i class="fa fa-trash-o text-danger fa-2x"></i></a>
                        </td>
                    </tr>

                @endfor

                </tbody>
            </table>
        </div>
    </div>
@stop