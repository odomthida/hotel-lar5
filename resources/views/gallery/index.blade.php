@extends('layout.admin.main')

{{--Section Content--}}
@section("content")

        <div class="row" id="photogallery">
            @foreach($photos as $photo)
            <div class="col-sm-2">
                <a href="/upload/{{$photo->path}}">
                    <img src="/upload/{{$photo->path}}" alt=""/>
                </a>
            </div>
                @endforeach
        </div>

@stop