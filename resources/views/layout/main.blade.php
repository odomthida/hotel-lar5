<html>
<header>
<title>

</title>
<link rel="stylesheet" href="{{URL('/')}}/css/bootstrap.min.css">
</header>
    <body>
        @section('layout.main')

        @show

        <div class="container">
        <nav class="navbar navbar-inverse">
        	<ul class="nav navbar-nav">
        		<li><a href="{{ URL::to('users') }}">View All Users</a></li>
        		<li><a href="{{ URL::to('users/create') }}">Create a User</a>
        	</ul>
        </nav>
            @yield('content')
        </div>
    </body>
</html>