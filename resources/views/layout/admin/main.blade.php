<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">

    <title>{{{ $title or "" }}} Administration</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
	<meta name="csrf-token" content="{{ csrf_token() }}" />
	<link rel="icon" href="{{asset('img/icon/favicon.ico')}}">
    <!-- Bootstrap 3.3.4 -->
	<link href="{{asset('css/bootstrap.css')}}" rel="stylesheet" type="text/css" />

    <!-- FontAwesome 4.3.0 -->
    <link href="{{asset('css/font-aswesom.css')}}" rel="stylesheet" type="text/css" />
    <!-- Ionicons 2.0.0 -->
    <link href="{{asset('css/ionicons.css')}}" rel="stylesheet" type="text/css" />
	<link href="{{asset('css/datatable.css')}}" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="{{asset('css/AdminLTE.css')}}" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->

    <!-- iCheck -->
    {{--<link href="plugins/iCheck/flat/blue.css" rel="stylesheet" type="text/css" />--}}
    <!-- Morris chart -->
    {{--<link href="plugins/morris/morris.css" rel="stylesheet" type="text/css" />--}}
    <!-- jvectormap -->
    {{--<link href="plugins/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />--}}
    <!-- Date Picker -->
    {{--<link href="plugins/datepicker/datepicker3.css" rel="stylesheet" type="text/css" />--}}
    <!-- Daterange picker -->
    {{--<link href="plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />--}}
    <!-- bootstrap wysihtml5 - text editor -->
    {{--<link href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />--}}

	<link href="{{asset('plugin/datepicker/css/bootstrap-datepicker.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('css/magnific-popup.css')}}" rel="stylesheet">
	<link href="{{asset('css/admin.css')}}" rel="stylesheet" type="text/css" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="{{asset('js/html5shive.js')}}"></script>
    <script src="{{asset('js/respond.js')}}"></script>
    <![endif]-->
</head>
<body>
<div class="wrapper">

    <header class="main-header">
        <!-- Logo -->
	        @include('layout.admin.patials.logo')
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
			@include('layout.admin.patials.menu')
        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
	    @include('layout.admin.patials.left_nav')
    </aside>

    <!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		@yield('content')
	</div><!-- /.content-wrapper -->
    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> Demo
        </div>
        <strong>Copyright &copy; 2014-2015 <a href="/">Asecro Hotel</a>.</strong> All rights reserved.
    </footer>
</div><!-- ./wrapper -->
<script src="{{asset('js/jquery.min.js')}}"></script>
<!-- Bootstrap 3.3.2 JS --> 
<script src="{{asset('js/bootstrap.min.js')}}" type="text/javascript"></script>

<script src="{{asset('plugin/datepicker/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('js/jquery.confirm.js')}}"></script>
<script src="{{asset('js/jquery.cookie.js')}}"></script>
<script src="{{asset('js/jquery.magnific-popup.js')}}"></script>

<script src="{{asset('js/admin.js')}}"></script>
<script src="{{asset('js/datatable.js')}}"></script>
</body>
</html>
