<!-- Sidebar toggle button-->
<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
	<span class="sr-only">Toggle navigation</span>
</a>
<div class="navbar-custom-menu">
		<!-- User Account: style can be found in dropdown.less -->
    <ul class="nav navbar-nav">
		<li class="dropdown user user-menu">
			<a href="#" class="dropdown-toggle" data-toggle="dropdown">
				<i class="fa fa-user"></i>
				<span class="hidden-xs">{{Auth::user()->firstName.' '. Auth::user()->lastName}}</span>
			</a>
			<ul class="dropdown-menu">
				<!-- Menu Footer-->
				<li class="user-footer">
					<div class="pull-left">
						<a href="/user/profile/{{{Auth::user()->id or ""}}}" class="btn btn-default btn-flat">Profile</a>
					</div>
					<div class="pull-right">
						<a href="/auth/logout" class="btn btn-default btn-flat">Sign out</a>
					</div>
				</li>
			</ul>
    </ul>
</div>