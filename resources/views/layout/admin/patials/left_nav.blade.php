<!-- sidebar: style can be found in sidebar.less -->
<section class="sidebar">
	<!-- sidebar menu: : style can be found in sidebar.less -->
	<ul class="sidebar-menu">
		<li class="active treeview">
			<ul class="treeview-menu">
				@if (Auth::user()->user_type_id == 3 )
                    <li class=""><a href="/booking/my-reservation"><i class="fa fa-th"></i> Reservation </a></li>
                @elseif(Auth::user()->user_type_id == 1)
                    <li><a href="/room"> <i class="fa fa-bed"></i> Room </a></li>
                    <li class=""><a href="/facility"></i> <i class="fa fa-star"></i> Facility </a></li>
                    <li class=""><a href="/gallery"></i> <i class="fa fa-photo"></i>  Photo Gallery </a></li>
                    <li class=""><a href="/booking/list-booking"><i class="fa fa-th"></i> Reservation </a></li>

                    <li class=""><a href="/admin/list-user"><i class="fa fa-users"></i> User Management </a></li>

                @else
                    <li><a href="/room"> <i class="fa fa-bed"></i> Room </a></li>
                    <li class=""><a href="/facility"></i> <i class="fa fa-star"></i> Facility </a></li>
                    <li class=""><a href="/gallery"></i> <i class="fa fa-photo"></i>  Photo Gallery </a></li>
                    <li class=""><a href="/booking/list-booking"><i class="fa fa-th"></i> Reservation </a></li>

                @endif


			</ul>
		</li>
	</ul>
</section>
<!-- /.sidebar -->