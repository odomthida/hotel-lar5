<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Homepage</title>
	<link rel="icon" href="{{asset('img/icon/favicon.ico')}}">
	<link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/flexslider.css')}}" rel="stylesheet">
    <link href="{{asset('css/jquery-ui.theme.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/jquery-ui.structure.css')}}" rel="stylesheet">
    <link href="{{asset('css/jquery-ui.css')}}" rel="stylesheet">
    <link href="{{asset('css/magnific-popup.css')}}" rel="stylesheet">


    <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">

    <script src="{{asset('js/html5shive.js')}}"></script>
    <script src="{{asset('js/respond.js')}}"></script>
</head>

<body>
    <div id="menu">
        <div class="container">
            <nav class="nav-top">
                  <ul class="nav navbar-nav navbar-right">
                       <li class="phone"><i class="glyphicon glyphicon-earphone"></i> +855 23 512 2888</li>
                       <li><a href="mailto:leang@asecro.com"><i class="glyphicon glyphicon-envelope"></i> leang@asecro.com</a></li>
                       <li class=""><a class="btn btn-link btn-xs" href="#booknow">Book now !</a></li>
                      @if(Auth::check())
                          <li class=""><a class="btn btn-link btn-xs" href="/auth/login">My page</a></li>
                      @else
                          <li class=""><a class="btn btn-link btn-xs" href="/auth/login">Login</a></li>
                      @endif
                  </ul>
            </nav>
            @include('layout.partials.menu')
        </div>
    </div>
    <div class="container" id="mainContain">
        @yield('content')
    </div>
    @include('layout.partials.footer')
<!-- /.container -->
    <script src="{{asset('js/jquery.min.js')}}"></script>
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/jquery.flexslider.js')}}"></script>
<script src="{{asset('js/datepicker.min.js')}}"></script>
<script src="{{asset('js/jquery.magnific-popup.js')}}"></script>
<script src="{{asset('js/main.js')}}"></script>

</body>
</html>