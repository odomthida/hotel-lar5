<!-- Footer -->
<footer>
	<div class="container">
		<div class="row home-footer">
			<div class="col-sm-3">
				<h4 class="title-footer">ABOUT US</h4>
				<span class="title-block"></span>

				<p class="p-footer">Asecro Hotel is strategically located on Chroy Changvar peninsula, opposite to the
					Royal Palace and approximately 13 km from Phnom Penh International Airport. Offering breathtaking
					view of the majestic Royal Palace.</p>
			</div>
			<div class="col-sm-3">
				<h4 class="title-footer">RECENT POST</h4>
				<span class="title-block"></span>
			</div>
			<div class="col-sm-3">
				<h4 class="title-footer">SIGN UP FOR NEWSLETTER</h4>
				<span class="title-block"></span>

				<div class="form-group footer-mail">
					<label class="col-md-4 control-label footer-mail " for="">Email:</label>
					<input id="" name="" type="text" placeholder="email" class="form-control input-md">
				</div>

				<!-- Button -->
				<div class="form-group">
					<label class="col-md-4 control-label SignUp " for=""></label>
					<button id="" name="" class="btn btn-primary ">Sign Up</button>
				</div>

			</div>
			<div class="col-sm-3">
				<h4 class="title-footer">SOCIAL MEDIA</h4>
				<span class="title-block"></span>

				<div class="social">
					<ul class="nav navbar-nav">
						<li><a href="https://www.facebook.com/asecro?fref=ts" target="_blank"><img src="{{asset('image/facebook.png')}}" width="40" alt="Facebook"/></a></li>
						<li><a href="https://twitter.com/asecro" target="_blank"><img src="{{asset('image/twitter.png')}}" width="40" alt="Twitter"/></a></li>
						<li><a href="https://plus.google.com/+Asecro/posts" target="_blank"><img src="{{asset('image/google.png')}}" width="40" alt="Google+"/></a></li>
					</ul>
				</div>
			</div>
		</div>

		<div class="row home-footer">
			<hr/>
			<div class="col-sm-12 title-footer">
				<p>Copyright &copy; Asecro 2015</p>
			</div>
		</div>
		<!-- /.row -->
	</div>
</footer>
