
<nav class="navbar" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand logo" href="/">Asecro Hotel</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="menu-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="{{ URL::to('/') }}">Home</a></li>
                <li><a href="{{URL::to('booking')}}">Reservation</a></li>
                <li><a href="{{URL::to('/')}}">Photgallery</a></li>
                <li><a href="#">Restaurant</a></li>
                <li><a href="{{ URL::to('contact') }}">Contact Us</a></li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
</nav>