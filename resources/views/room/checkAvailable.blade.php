@extends('layout.homepage')
@section("content")

        <div class="row">
            <div class="col-sm-4 ">
                    <div class="checkavaileble">
                        <h4>Your reservation</h4>
                        <i class="title-underline"></i>
                        <ul>
                            <li>Check In :</li>
                            <li>Check Out :</li>
                            <li>Guest :</li>
                        </ul>
                        <a class="btn btn-primary btn-xs" href="#" >Edit Reservation</a>
                    </div>
            </div>
            <div class="col-sm-8">
                    <div class="checkavaileble">
                        <h4 class="title-footer">Chose your room</h4>
                        <i class="title-underline"></i>
                        <hr/>
                        <div class="row">
                            <h4 class="title-footer">Single Room</h4>
                        </div>
                        <div class="row">
                            <div class="col-sm-4 teaser availeble-left">
                                <img src="{{asset('image/single-room.jpg')}}" alt=""/>
                            </div>
                            <div class="col-sm-8 availeble">
                                <ul>
                                    <li>Number of room :</li>
                                    <li>Occupancy :</li>
                                    <li>Size :</li>
                                    <li>From per night :</li>
                                </ul>
                            </div>
                        </div>
                        <hr/>
                        <div class="row">
                            <h4 class="title-footer">Couple Room</h4>
                        </div>
                        <div class="row">
                            <div class="col-sm-4 teaser availeble-left">
                                <img src="{{asset('image/single-room.jpg')}}" alt=""/>
                            </div>
                            <div class="col-sm-8 availeble">
                                <ul>
                                    <li>Number of room :</li>
                                    <li>Occupancy :</li>
                                    <li>Size :</li>
                                    <li>From per night :</li>
                                </ul>
                            </div>
                        </div>
                        <hr/>
                        <div class="row">
                            <h4 class="title-footer">Double Room</h4>
                        </div>
                        <div class="row">
                            <div class="col-sm-4 teaser availeble-left">
                                <img src="{{asset('image/single-room.jpg')}}" alt=""/>
                            </div>
                            <div class="col-sm-8 availeble">
                                <ul>
                                    <li>Number of room :</li>
                                    <li>Occupancy :</li>
                                    <li>Size :</li>
                                    <li>From per night :</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
        <hr/>
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="row">
                    <div class="col-md-6">
                        <h3>Leave a comment</h3>
                        <!-- Text input-->
                        <div class="form-group">
                            <label class="control-label" for="">Username:</label>
                            <input id="" name="" type="text" placeholder="Username" class="form-control input-md" required="">
                        </div>

                        <!-- Text input-->
                        <div class="form-group">
                            <label class="control-label" for="">Email:</label>
                            <input id="" name="" type="text" placeholder="Email" class="form-control input-md" required="">
                        </div>

                        <!-- Text input-->
                        <div class="form-group">
                            <label class="control-label" for="">Website:</label>
                            <input id="" name="" type="text" placeholder="Website" class="form-control input-md" required="">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <!-- Textarea -->
                        <div class="form-group">
                            <label class="control-label" for="">Comment:</label>
                            <textarea placeholder="comment" class="form-control" ></textarea>
                        </div>

                        <!-- Button -->
                        <div class="form-group">
                            <label class="control-label" for=""></label>
                            <button id="" name="" class="btn btn-primary">Submit comment</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @stop

