@extends('layout.admin.main')
@section('content')
<div class="roomlist gallery">
    <div class="col-md-10 ">
        <div class="row">
            <div class="col-md-6">
                <h2>Picture for room: {{$room->name}}</h2>
                <table class="table table-condensed table-bordered">
                    <tbody>
                    <tr>
                        <td>Room Type:</td>
                        <td>{{$room->roomtype->name}}</td>
                    </tr>
                    <tr>
                        <td>Facility: </td>
                        <td>{!! $room->facility->description !!}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-md-6">
                <br/><br/><br/><br/><br/><br/>
                <form action="/room/gallery" method="post" enctype="multipart/form-data">
                    <input class="hidden" id="fileupload" type="file" name="photos[]" data-url="/room/add-image" multiple>
                    <input type="hidden" name="id" value="{{$room->id}}"/>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                </form>
                <a href="#add_Gallery" id="btnFileUpload" class="btn btn-primary addImageBtn"> <i class="fa fa-plus"></i> Add Image</a>
                <a href="#Delete" id="deleteImageBtn" class="btn btn-danger "> <i class="fa fa-trash"></i> Delete Image</a>
                <a href="#cancel" id="cancelImage" class="btn btn-warning"> <i class="fa fa-times"></i> Cancel</a>
            </div>
            </div>
			<div class="row">
				<p class="text text-info">Pictures: </p>
			</div>
			<div class="row room-picture">
                @foreach( $room->photos as $photo )
					<div class="col-sm-2">
                        <i class="confirm fa fa-2x fa-times-circle-o" data-action="/room/delete-gallery" data-record="{{$photo->id}}"></i>
                        <a href="/upload/{{$photo->path}}">
						    <img src="/upload/{{$photo->path}}" alt="" width="100%"/>
                        </a>
					</div>
				@endforeach
            </div>
		</div>
	</div>
@endsection
