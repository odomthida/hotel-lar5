@extends('layout.admin.main')

{{--Section Content--}}
@section("content")
    <!-- Page Content -->
    <div class="row roomlist">
        <div class="col-lg-6">
            <h2> Edit Room</h2>
            <hr/>
            {!! Form::open(array('url' => '/room/edit-room/'.$activeRoom->id, 'method' => 'POST')) !!}
            <div class="form-group">
                {!! Form::label('name', 'Name' , array('class' => '')) !!}
                {!! Form::text('name', "$activeRoom->name", array('class' => 'form-control', 'placeholder'=>"Room Name", "required" => "")) !!}
            </div>

            <div class="form-group">
                {!! Form::label('roomtype', 'Type' , array('class' => '')) !!}
                {!! Form::select('roomtype', $rooms  , $activeRoom->roomtype->id, array('class' => 'form-control')) !!}
            </div>
            {{--Start Adult and children--}}
            <div class="row">
                <div class="form-group col-md-6">
                    {!! Form::label('adult', 'Adult' , array('class' => '')) !!}
                    {!! Form::select('adult', array(1,2,3,4,5,6), $activeRoom->adult -1, array('class' => 'form-control')) !!}
                </div>

                <div class="form-group col-md-6">
                    {!! Form::label('child', 'Child' , array('class' => '')) !!}
                    {!! Form::select('child',  array(1,2,3,4,5,6), $activeRoom->child - 1, array('class' => 'form-control')) !!}
                </div>
            </div>
            <div class="form-group">
                {!! Form::label('price', 'Price' , array('class' => '')) !!}
                {!! Form::text('price', $activeRoom->price, array('class' => 'form-control', 'placeholder'=>"Price")) !!}
            </div>
			
            <div class="form-group">
                {!! Form::label('facility', 'Type' , array('class' => '')) !!}
                {!! Form::select('facility', $facilities, $activeRoom->facility->id, array('class' => 'form-control')) !!}
            </div>

            <div class="form-group">
                {!! Form::label('description', 'Description' , array('class' => '')) !!}
                {!! Form::textarea('description', $activeRoom->description , array('class' => 'form-control', 'placeholder'=>"Description")) !!}
            </div>

            {!! Form::submit('Submit!', array('class' => 'btn btn-primary')) !!}
            <a href="/room" class="btn btn-primary"><i class="fa fa-th-list"></i> List</a>

            {!! Form::close()!!}
        </div>
    </div>
@stop