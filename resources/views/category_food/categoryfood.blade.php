@extends('...layout.homepage')

{{--Section Content--}}
@section("content")
    <div class="row">
        <div class="col-md-6 col-md-offset-3">

            <form action="/category_food/category-food" method="post" style="margin: 50px 0;">
                <div class="form-group">
                    <label>Country Name</label>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="text" class="form-control" name="country_name" placeholder="Country Name">
                </div>
                <div class="form-group">
                    <label>Food Type</label>
                    <input type="text" class="form-control" name="type_food" placeholder="Food Type">
                </div>

                <div class="form-group">
                    <label>Status</label>
                    <input type="text" class="form-control" name="status" placeholder="Status">
                </div>

                <button type="submit" class="btn btn-default">Submit</button>
            </form>
        </div>
    </div>
@stop