@extends('layout.login')

{{--Section Content--}}
@section("content")

    <div class="row">
	    <div class="col-md-8 col-md-offset-2">
		    <div class="alert alert-info alert-dismissible" role="alert">
			    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			    <p>Please check your email for Confirmation code!
				    <br/>
				    or Resend the link below.
			    </p>
		    </div>

		    <div class="panel panel-default">
		        <div class="panel-heading">Reset Password</div>
		        <div class="panel-body">
	        <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/resend-code') }}">
		        <input type="hidden" name="_token" value="{{ csrf_token() }}">

		        <div class="form-group">
			        <label class="col-md-4 control-label">E-Mail Address</label>
			        <div class="col-md-6">
				        <input type="hidden" name="_token" value="{{ csrf_token() }}">
				        <input type="email" class="form-control" name="email" value="{{ old('email') }}">
			        </div>
		        </div>

		        <div class="form-group">
			        <div class="col-md-5 col-md-offset-4">
				        <button type="submit" class="btn btn-primary">
					        Send Activation Link
				        </button>
			        </div>
		        </div>
	        </form>
		        </div>
		        </div>
		        </div>
        </div>
    </div>
@stop