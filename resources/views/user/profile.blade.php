@extends('layout.admin.main')

{{--Section Content--}}
@section("content")
	<br/>
	<div class="row user-profile">
		<div class="col-sm-8 col-sm-offset-2 " >
			<div class="panel panel-info">
				<div class="panel-heading">
					<h3 class="panel-title">{{$user->firstName.' '.$user->lastName}}</h3>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-xs-3 col-sm-3 " align="center">
							<div class="profile-pic">
								<img class=" profile-img img-circle" alt="User Pic" src="{{{$user->thumbnail or asset('img/avatar.png')}}}" >
								<a class="upload" href="#uploadImage"> Upload</a>
							</div>
						</div>
						<div class=" col-xs-9 col-sm-9 ">
							<table class="table table-user-information">
								<tbody>
								<tr>
									<td>Date of Birth</td>
                                    @if ($user->dob)
                                        {{{! $date = date('m/d/Y',date_create_from_format('Y-m-d',$user->dob)->getTimestamp())}}}
                                    @else
                                        {{{! $date=""}}}
                                    @endif
									<td><input class="disabled input datepicker" name="dob" type="text" value="{{$date}}" disabled placeholder="Date Of Birth"/> </td>
								</tr>

								<tr>
								<tr>
									<td>Gender</td>
									<td>
										<select name="gender" class="input disabled" disabled>
											<option value="0"> - Select Gender - </option>
											<option value="male" {{$user->gender=="male"? "selected":""}}> Male </option>
											<option value="female" {{$user->gender=="female"? "selected":""}} > Female</option>
										</select>
								</tr>
								<tr>
									<td>Email</td>
									<td><input type="text" class="disabled" name="email" placeholder="Email" disabled value="{{$user->email}}"/></td>
								</tr>
								<td>Phone Number</td>
								<td><input type="text"  class="disabled input"  name="phone" placeholder="Phone Number" disabled value="{{$user->phone}}"/>
								</td>

								</tr>
								<tr>
									<td>Address</td>
									<td><textarea name="address" class="disabled input" cols="" rows="5" disabled placeholder="Address">{{$user->address}}</textarea></td>
								</tr>

								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="panel-footer">
					<a data-original-title="Remove this user" data-toggle="tooltip" type="button" class="cancel btn btn-sm btn-danger disabled"><i class="glyphicon glyphicon-remove"></i></a>
						<span class="pull-right">
							<a href="#" data-original-title="Edit this user" data-toggle="tooltip" type="button" class="edit btn btn-sm btn-warning"><i class="glyphicon glyphicon-edit"></i></a>
							<a href="#" data-original-title="Save" data-toggle="tooltip" type="button" class="save btn btn-sm btn-primary disabled"><i class="fa fa-floppy-o"></i></a>
						</span>
				</div>

			</div>
		</div>
	</div>
@stop