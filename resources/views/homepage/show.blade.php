@extends('layout.homepage')
@section("content")
    <!-- Full Page Image Background Carousel Header -->
    <div id="slider">
        <div class="slider">
            <ul class="slides">
                <li>
                    <img src="{{asset('image/slide1.jpg')}}" alt=""/>
                </li>
                <li>
                    <img src="{{asset('image/slide2.jpg')}}" alt=""/>
                </li>
                <li>
                     <img src="{{asset('image/slide3.jpg')}}" alt=""/>
                </li>
            </ul>
        </div>
        <ul class="flex-direction-nav">
            <li><a class="flex-prev" href="#"></a></li>
            <li><a class="flex-next" href="#"></a></li>
        </ul>
    </div>

    <div class="row check">
        <div class="home-reservation-box">
            <form class="form-group booking-form" action="/booking/check-available" >
                <div class="col-sm-3">
                    <input type="text" id="datefrom" name="dateFrom" placeholder="Check In" class="datepicker">
                 </div>
                <div class="col-sm-3">
                    <input type="text" id="dateto" name="dateTo" placeholder="Check Out" class="datepicker"/>
                </div>
                <div class="col-sm-2">
                    <input type="hidden" name="room_qty" id="room_qty" value="1"/>
                    <div class="select-wrapper">
                        <select id="room_1_adults" name="adult" placeholder="Adult">
                            <option value="adult">Adult</option>
                            <option value="0">0</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="select-wrapper">
                        <select id="room_1_children" name="children">
                            <option value="children" selected>Children</option>
                            <option value="0">0</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-2">
                    <input type="submit" value="Check Available" class="btn btn-info" />

                </div>
            </form>
        </div>
    </div>

	<div class="teaser-container">
	    <div class="row">
	        <div class="col-sm-4 teaser">
                <a href="/room/detail/1">
	                <h4>Single Room</h4>
	                <img src="{{asset('image/single-room.jpg')}}" alt=""/>
                </a>
	        </div>
	        <div class="col-sm-4 teaser">
                <a href="/room/detail/2">
                    <h4>Couple Room</h4>
                    <img src="{{asset('image/Couple-room.jpg')}}">
                </a>
	        </div>
	        <div class=" col-sm-4 teaser">
                <a href="/room/detail/3">
                    <h4>Double Room</h4>
                    <img src="{{asset('image/Double-room.jpg')}}">
                </a>
	        </div>
	    </div>
	    <br/>
	    <br/>

	    <div class="row">
	            <div class="col-sm-4 teaser">
	                    <h4 class="about">Asecro Hotel</h4>
	                    <p>Asecro Hotel is strategically located on Chroy Changvar peninsula, opposite to the Royal Palace and approximately 13 km from Phnom Penh International Airport. Offering breathtaking view of the majestic Royal Palace.</p>
	            </div>
	            <div class="col-sm-4  teaser">
	                <h4 class="about">Asecro Hotel Reviwer</h4>
	                <ul>
	                    <li>99 luxury guest rooms</li>
	                    <li>Michelin star restaurant</li>
	                    <li>Rooftop cocktail bar</li>
	                    <li>Infinity pool</li>
	                    <li>24 hour front desk staff</li>
	                    <li>Free airport pick up/drop off service</li>
	                </ul>
	            </div>
	            <div class="col-sm-4 teaser">
	                <h4>Asecro Hotel Location</h4>
	                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3908.9181360528114!2d104.93063120000001!3d11.557726300000017!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31095131197d9fb3%3A0x5725697d0617633e!2s19%2C+Phnom+Penh!5e0!3m2!1sen!2skh!4v1426323438719" width="100%" height="150" frameborder="0" style="border:0"></iframe>
	            </div>
	    </div>
	</div>


@stop