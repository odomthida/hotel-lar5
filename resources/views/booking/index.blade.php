@extends('layout.homepage')

{{--Section Content--}}
@section("content")

    @foreach($roomtypes as $roomtype)
        <h1 class="title">{{$roomtype->name}}</h1>
        @foreach ($roomtype->room as $room)
            @if(isset($room->photos[0]))
                <div class="room">
                    <div class="row">
                        <div class="col-sm-6 magnificPopup">
                            <div class="main-picture">
                                <a href="/upload/{{$room->photos[0]->path}}" data-effect="mfp-zoom-in" data-mfp-src="/upload/{{$room->photos[0]->path}}" >
                                    <img src="/upload/{{$room->photos[0]->path}}" alt="" width="100%"/>
                                </a>
                            </div>
                            {{!! $test=false }}
                            @foreach( $room->photos as $photo)
                                @if($test)
                                    <a href="/upload/{{$photo->path}}" data-effect="mfp-zoom-in" data-mfp-src="/upload/{{$photo->path}}" >
                                        <img src="/upload/{{$photo->path}}" alt="" width="50"/>
                                    </a>
                                @endif
                                {{! $test=true }}
                            @endforeach
                        </div>
                        <div class="col-sm-6 detail-room">
                            <h2>{{ $room->name }}</h2>
                            <table class="table table-bordered">
                                <tbody>
                                <tr>
                                    <td>Adult: </td>
                                    <td>{{$room->adult}}</td>
                                </tr>
                                <tr>
                                    <td>Child: </td>
                                    <td>{{$room->child}}</td>
                                </tr>
                                <tr>
                                    <td>Price: </td>
                                    <td>$ {{$room->price}} / night</td>
                                </tr>
                                <tr>
                                    <td>Type: </td>
                                    <td>{{$room->roomtype->name}} / night</td>
                                </tr>
                                <tr>
                                    <td>Facility: </td>
                                    <td> <?php echo $room->facility->description ?></td>
                                </tr>


                                </tbody>
                            </table>

                            <div class="col-sm-12" >
                                <h2 class="visible-sm-4"></h2>
                                <p>{{$room->description}}</p>
                                <a href="/booking/room/{{$room->id}}" class="btn btn-info"> Book now ! </a>
                            </div>
                        </div>
                    </div>
                </div>
                <hr/>
            @endif
        @endforeach
    @endforeach
@stop