@extends('layout.admin.main')

{{--Section Content--}}
@section("content")
    <!-- Page Content -->
    <div class="row roomlist">
        <div class="col-lg-10">
            <h1>Booking List</h1>
            <table class="table table-bordered datatable">
                <thead>
                <tr>
                    <th>No</th>
                    <th>Email</th>
                    <th>Room Name</th>
                    <th>Check In</th>
                    <th>Check Out</th>
                    <th>Other</th>
                </tr>
                </thead>
                <tbody>
                @for($i=0; $i<count($bookings); $i++ )
                    <tr>
                        <td>{{$i + 1}}</td>
                        <td> {{ $bookings[$i]->user->email }}</td>
                        <td> {{ $bookings[$i]->room->name}}</td>
                        <td> {{date('d/m/Y', $bookings[$i]->startDate)}}</td>
                        <td> {{date('d/m/Y', $bookings[$i]->endDate)}}</td>
                        <td>
                            <a href="/booking/edit-booking/{{$bookings[$i]->id}}"><i class="fa fa-edit fa-2x"></i></a>
                            <a href="#disabled" class="confirm" data-action="/booking/delete-booking" data-record="{{$bookings[$i]->id}}" title="Hide Item"><i class="fa fa-times text-danger fa-2x"></i></a>
                        </td>
                    </tr>
                @endfor
                </tbody>
            </table>
        </div>
    </div>
@stop