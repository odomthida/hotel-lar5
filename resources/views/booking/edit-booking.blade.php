@extends('layout.admin.main')

{{--Section Content--}}
@section("content")
    <!-- Page Content -->
    <div class="row roomlist">
        <div class="col-lg-10">
           <h1>List Booking </h1>
            {!! Form::open(array('url' => '/booking/edit-booking', 'method' => 'POST')) !!}
            <input type="hidden" name="id" value="{{$booking->id}}"/>
            <div class="form-group">
                {!! Form::label('checkin', 'Check In' , array('class' => '')) !!}
                {!! Form::text('checkin', date('m/d/Y', $booking->startDate), array('class' => 'form-control datepicker', 'placeholder'=>"Check in date", "required" => "")) !!}
            </div>
            <div class="form-group">
                {!! Form::label('checkout', 'Check Out' , array('class' => '')) !!}
                {!! Form::text('checkout', date('m/d/Y', $booking->endDate), array('class' => 'form-control datepicker', 'placeholder'=>"Check out date", "required" => "")) !!}
            </div>

            <input type="submit" value="Update" class="btn btn-info"/>
            <br/><br/>

            {!! Form::close()!!}

        </div>
    </div>
@stop