@extends('layout.homepage')

{{--Section Content--}}
@section("content")
    <!-- Page Content -->
    <div class="row roomlist">
        <br/><br/>
        <div class="col-sm-8 col-sm-offset-2">
            <p class="alert alert-success">Congratulation your booking have been registered successful !</p>
            <p class="alert alert-success"> Please check your email for your booking credentials.</p>
        </div>
    </div>
@stop