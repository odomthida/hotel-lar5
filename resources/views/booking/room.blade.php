@extends('layout.homepage')

{{--Section Content--}}
@section("content")
    <!-- Page Content -->
    <h1 class="title"> Booking Information </h1>
    <div class="row roomlist">
        <div class="col-lg-4">
            {!! Form::open(array('url' => '/booking/check-available', 'method' => 'POST')) !!}
                <div class="form-group">
                    {!! Form::label('name', 'Name: Mr. / Mrs. / Ms. ' , array('class' => '')) !!}
                    {!! Form::text('name', "", array('class' => 'form-control', 'placeholder'=>"Name", "required" => "")) !!}
                    <input type="hidden" value="{{$room->id}}" name="id"/>
                </div>
                <div class="form-group">
                    {!! Form::label('email', 'Email' , array('class' => '')) !!}
                    {!! Form::email('email', "", array("class"=>"form-control", 'placeholder' => 'Email', 'required'=>"")); !!}
                </div>
                <div class="form-group">
                    {!! Form::label('occupation', 'Occupation' , array('class' => '')) !!}
                    {!! Form::text('occupation', "", array('class' => 'form-control', 'placeholder'=>"Occupation", "required" => "")) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('address', 'Address' , array('class' => '')) !!}
                    {!! Form::text('address', "", array('class' => 'form-control', 'placeholder'=>"Name", "required" => "")) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('checkin', 'Check In' , array('class' => '')) !!}
                    {!! Form::text('checkin', isset($userinfo['dateFrom'])?$userinfo['dateFrom']:"", array('class' => 'form-control datepicker', 'placeholder'=>"Check in date", "required" => "")) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('checkout', 'Check Out' , array('class' => '')) !!}
                    {!! Form::text('checkout', isset($userinfo['dateTo'])?$userinfo['dateTo']:"", array('class' => 'form-control datepicker', 'placeholder'=>"Check out date", "required" => "")) !!}
                </div>

                    <div class="form-group">
                        {!! Form::label('adult', 'Adult' , array('class' => '')) !!}
                        {!! Form::select('adult', $maxAdult, isset($userinfo['adult'])?$userinfo['adult']:"", array('class' => 'form-control')) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('child', 'Child' , array('class' => '')) !!}
                        {!! Form::select('child',  $maxChild, isset($userinfo['children'])?$userinfo['children']:"", array('class' => 'form-control')) !!}
                    </div>
            <input type="submit" value="Book" class="btn btn-info"/>
            <br/><br/>

            {!! Form::close()!!}
        </div>
        <div class="col-lg-8 list-room-book">
           @if(isset($room->photos[0]))
                    <div class="room">
                        <div class="row">
                            <div class="col-sm-6 magnificPopup">
                                <div class="main-picture">
                                    <a href="/upload/{{$room->photos[0]->path}}" data-effect="mfp-zoom-in" data-mfp-src="/upload/{{$room->photos[0]->path}}" >
                                        <img src="/upload/{{$room->photos[0]->path}}" alt="" width="100%"/>
                                    </a>
                                </div>
                                {{!! $test=false }}
                                @foreach( $room->photos as $photo)
                                    @if($test)
                                        <a href="/upload/{{$photo->path}}" data-effect="mfp-zoom-in" data-mfp-src="/upload/{{$photo->path}}" >
                                            <img src="/upload/{{$photo->path}}" alt="" width="50"/>
                                        </a>
                                    @endif
                                    {{! $test=true }}
                                @endforeach
                            </div>
                            <div class="col-sm-6 detail-room">
                                <h2>{{ $room->name }}</h2>
                                <table class="table table-bordered">
                                    <tbody>
                                    <tr>
                                        <td>Adult: </td>
                                        <td>{{$room->adult}}</td>
                                    </tr>
                                    <tr>
                                        <td>Child: </td>
                                        <td>{{$room->child}}</td>
                                    </tr>
                                    <tr>
                                        <td>Price: </td>
                                        <td>$ {{$room->price}} / night</td>
                                    </tr>
                                    <tr>
                                        <td>Type: </td>
                                        <td>{{$room->roomtype->name}} / night</td>
                                    </tr>
                                    <tr>
                                        <td>Facility: </td>
                                        <td> <?php echo $room->facility->description ?></td>
                                    </tr>
                                    </tbody>
                                </table>

                            <div class="col-sm-12" >
                                <h2 class="visible-sm-4"></h2>
                                <p>{{$room->description}}</p>
                            </div>
                            </div>
                        </div>
                    </div>
                    <hr/>
                @endif
        </div>
                <hr/>
        <br/><br/><br/>
    </div>
@stop