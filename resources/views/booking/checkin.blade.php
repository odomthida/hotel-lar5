@extends('layout.homepage')

{{--Section Content--}}
@section("content")
    <div class="row">
        <div class="col-md-6 col-md-offset-3">

            <form action="/booking/check-in" method="post" style="margin: 50px 0;">
                <div class="form-group">
                    <label for="exampleInputEmail1">Email address</label>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="email" class="form-control" name="email" placeholder="Enter email">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Password</label>
                    <input type="password" class="form-control" name="password" placeholder="Password">
                </div>
                <button type="submit" class="btn btn-default">Submit</button>
            </form>
        </div>
    </div>
@stop