@extends('layout.admin.main')

{{--Section Content--}}
@section("content")
    <!-- Page Content -->
    <div class="row roomlist">
        <div class="col-lg-6">
            <h2> Create Facility</h2>
            <hr/>
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            {!! Form::open(array('url' => '/facility/add-facility', 'method' => 'POST')) !!}
            <div class="form-group">
                {!! Form::label('name', 'Name' , array('class' => '')) !!}
                {!! Form::text('name',  old('name'), array('class' => 'form-control', 'placeholder'=>"Facility Name", 'required'=>""   )) !!}
            </div>

            <div class="form-group">
                {!! Form::label('desciption', 'Descirption' , array('class' => '')) !!}
                {!! Form::textarea('description', old('description'), array('class' => 'form-control', 'placeholder'=>"Description")) !!}
            </div>
            {!! Form::submit('Submit!', array('class' => 'btn btn-primary')) !!}

            {!! Form::close()!!}
        </div>
    </div>
@stop