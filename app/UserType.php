<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
/**
 * Class UserType
 * @package App
 */
class UserType extends Model {

	//
    protected $fillable = ['name'];

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    /**
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function user() {
        return $this->hasMany('App\User');
    }
}
