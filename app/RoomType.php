<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Room;
class RoomType extends Model {


    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = ['name'];

    public function room() {
        return $this->hasMany('App\Room', 'id_room_type');
    }
}
