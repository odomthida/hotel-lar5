<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Photogallery extends Model {

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = ['thumbnail', 'path', 'id_room'];

    public function room() {
        return $this->belongsTo('App\Room', 'id_room');
    }
}
