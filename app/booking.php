<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class booking extends Model {

    use SoftDeletes;

    protected $dates = ['deleted_at'];


    public function user() {
        return $this->belongsTo('App\User', 'id_user');
    }

    public function room() {
        return $this->belongsTo('App\Facility', 'id_room');
    }
}
