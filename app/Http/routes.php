<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
	'booking' => 'BookingController',
	'category_food'=>'Category_foodController',
	'admin'=>'AdminController',
	'user'=>'UserController',
	'room' => 'RoomController',
	'facility'=>'RoomFacilityController',
	'gallery' => 'PhotogalleryController'
]);


Route::get('/',  'WelcomeController@show');
Route::get('contact', 'WelcomeController@showContact');
Route::get('checkAvailable', 'WelcomeController@showAvailableRoom');
