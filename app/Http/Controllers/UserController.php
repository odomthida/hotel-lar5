<?php namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\User;

class UserController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth', ['except' => 'getCheckEmail']);
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function getIndex()
	{
		return view('user.index');
	}

	public function getProfile( $id = null) {
		$title = "Profile";
		$data['title'] = $title;
		$data['user'] = Auth::user();
		return view('user.profile', $data);

	}

	public function postUpdateUser(Request $request) {
		if ($request->ajax())
		{

			$user = Auth::user();
			$user->address= $request->get('address');
			$user->phone = $request->get('phone');
//			dd($request->get('gender'));
			$user->gender = $request->get('gender');
			$user->dob = date_create_from_format('m/d/Y', $request->get('dob'));
			$user->save();

			$contents = array("message"=>"success");
			return response()->json($contents);
		}
	}

	public function getCheckEmail() {
//		dd('test');
		return view('user.check-email');
	}

}
