<?php
namespace App\Http\Controllers;

use App\Facility;
use Illuminate\Http\Request;

class RoomFacilityController extends Controller {


	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
    }

	public function  getIndex() {

		$title="Facility";
		$data["title"] = $title;
		$data['facility'] = Facility::all();
		return view('facility.index', $data);
	}

	public function getAddFacility () {
		$title="Add Facility";
		$data["title"] = $title;
		return view('facility.add-facility', $data);
    }

    public function postAddFacility (Request $request) {
        $all = $request->all();

        $facility = array(
            "name" => $all['name'],
            "description" => $all['description']
        );

        Facility::create($facility);
        return redirect('/facility');
    }

    public function postDeleteFacility(Request $request) {
        $id = $request->get('id');
        if ($id) {
            $record = Facility::find($id);
            $record->delete();
        }
    }

    public function getEditFacility ($id=null) {
        if (is_null($id)) return redirect("/facility");
        $data['title'] = "";
        $data['facility'] = Facility::find($id);
        return view('facility.edit-facility', $data);
    }

    public function postEditFacility (Request $request) {
        $all = $request->all();
        $id = $all['id'];
        if (is_null($id)) return redirect("/facility");

        $facility = Facility::find($id);
        $facility->name = $all['name'];
        $facility->description = $all['description'];
        $facility->save();
        return redirect('/facility');
    }

}
