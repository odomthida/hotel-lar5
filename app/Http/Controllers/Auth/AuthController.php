<?php namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\User;

class AuthController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Registration & Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users, as well as the
	| authentication of existing users. By default, this controller uses
	| a simple trait to add these behaviors. Why don't you explore it?
	|
	*/

    
    protected $auth;
    protected $registrar;
    public function getRegister()
    {
        return view('auth.register');
    }
    public function postRegister(Request $request)
    {
        $validator = $this->registrar->validator($request->all());
        if ($validator->fails()) {
            $this->throwValidationException($request, $validator);
        }
        $user=$this->registrar->create($request->all());
		$this->sendConfirmCode($user);
        return view('/auth/login')  ;
    }

    public function getLogin() {
        return view('auth.login');
    }

    public function postLogin(Request $request)
    {
        $this->validate($request, array('email' => 'required|email', 'password' => 'required'));
        $credentials = $request->only('email', 'password');

	    if(User::whereRaw("email='".$credentials['email']."' and active=0")->count()) {
		       return redirect('/user/check-email');
	    } else {
		    if ($this->auth->attempt($credentials, $request->has('remember'))) {
			    return redirect()->intended($this->redirectPath());
		    }
		}
		return redirect($this->loginPath())->withInput($request->only('email', 'remember'))->withErrors(array('email' => $this->getFailedLoginMessage()));
	}

    protected function getFailedLoginMessage()
    {
        return 'These credentials do not match our records.';
    }

    public function getLogout()
    {
        $this->auth->logout();
        return redirect(property_exists($this, 'redirectAfterLogout') ? $this->redirectAfterLogout : '/auth/login');
    }

    public function redirectPath() {
        if (property_exists($this, 'redirectPath')) {
            return $this->redirectPath;
        }
        return property_exists($this, 'redirectTo') ? $this->redirectTo : '/admin';
    }

    public function loginPath() {
        return property_exists($this, 'loginPath') ? $this->loginPath : '/auth/login';
    }


	public function sendConfirmCode($user) {
		Mail::send('emails.password', ['token' => $user->activation_code, 'userId' => $user->id], function($message) use ($user)
		{
			$message->to($user->email, $user->username)->subject('Confirmation Code!');
		});
	}

	/**
	 * Create a new authentication controller instance.
	 *
	 * @param  \Illuminate\Contracts\Auth\Guard  $auth
	 * @param  \Illuminate\Contracts\Auth\Registrar  $registrar
	 * @return void
	 */
	public function __construct(Guard $auth, Registrar $registrar)
	{

        $this->auth = $auth;
		$this->registrar = $registrar;
		$this->middleware('guest', ['except' => 'getLogout']);
	}

}
