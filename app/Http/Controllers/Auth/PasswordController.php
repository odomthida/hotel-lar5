<?php namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\PasswordBroker;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Mail;

class PasswordController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Password Reset Controller
	|--------------------------------------------------------------------------
	|
	| This controller is responsible for handling password reset requests
	| and uses a simple trait to include this behavior. You're free to
	| explore this trait and override any methods you wish to tweak.
	|
	*/

	use ResetsPasswords;

	/**
	 * Create a new password controller instance.
	 *
	 * @param  \Illuminate\Contracts\Auth\Guard  $auth
	 * @param  \Illuminate\Contracts\Auth\PasswordBroker  $passwords
	 * @return void
	 */
	public function __construct(Guard $auth, PasswordBroker $passwords)
	{
		$this->auth = $auth;
		$this->passwords = $passwords;

		$this->middleware('guest');
	}

	public function postResendCode() {
		Mail::send('emails.password', ['token' => 'tokenkey'], function($message)
		{
			$message->to('bidam.neo@gmail.com', 'John Smith')->subject('Welcome!');
		});
	}

	public function getConfirmCode($resetCode="", $userid="") {

		$user = User::find($userid);

		if ($user->activation_code == $resetCode) {
			$user->active=1;
			$user->update();
			return redirect('/auth/login');
		} else {
			dd("Code expired Please resend the code again!!! ");
		}

	}

}
