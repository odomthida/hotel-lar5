<?php namespace App\Http\Controllers;

use App\booking;
use App\category_food;
use App\RoomType;
use App\User;
use Illuminate\Http\Request;
use App\Room;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class BookingController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Welcome Controller
    |--------------------------------------------------------------------------
    |
    | This controller renders the "marketing page" for the application and
    | is configured to only allow guests. Like most of the other sample
    | controllers, you are free to modify or remove it as you desire.
    |
    */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }


    /**
     * Show the application welcome screen to the user.
     *
     * @return Response
     */
    public function getIndex($arg1=null, $arg2=null)
    {
        $data['roomtypes'] = RoomType::all();

        return view('booking.index', $data);
    }


    /**
     * @return \Illuminate\View\View
     */
    public function getCheckIn() {
        return view ("booking.checkin");
    }

    /**
     *
     */
    public function postCheckIn(Request $request) {
        var_dump($request->all());
        die();
    }

    public function getBookRoom($id=null) {
        if (is_null($id)) return redirect();

    }

    public function getCheckAvailable( Request $request) {
        $data['title'] = "Check Available";
        $data['userinfo'] = $request->all();
        $data['rooms'] = Room::all();
        return view('booking.check-available', $data);
    }


    public function postCheckAvailable(Request $request) {
        $all = $request->all();

        $checkIn = date_create_from_format('m/d/Y',$all['checkin']);
        $checkOut = date_create_from_format('m/d/Y',$all['checkout']);

        $room = Room::find($all['id']);
        $user = User::where('email','=', $all['email'])->get();

        if ( ! count($user)) {
            $userinfo = array(
                    'firstName' => "",
                    'lastName' => "",
                    'activation_code' => "",
                    'username' => $all['name'] . time() ,
                    'email' => $all['email'],
                    'password' => bcrypt($all['name']),
                    'user_type_id' => 3,
                    'active'=>1
            );
            $user = User::create($userinfo);
        } else {
            $user=$user[0];
        }

        $booking = new booking();
        $booking->id_user = $user->id;
        $booking->id_room = $room->id;
        $booking->startDate= $checkIn->getTimestamp();
        $booking->endDate= $checkOut->getTimestamp();
        $booking->save();

        $this->sendConfirmCode($user, $all['name']);
        return redirect('/booking/success');
    }

    public function getListRooms() {
        $data['rooms'] = Room::all();
        return view('booking.check-available', $data);
    }

    public function getRoom($id=null, Request $request) {
        if (is_null($id)) return redirect('/booking');
        $data['title'] = "Booking";
        $room = Room::find($id);
        $data['userinfo'] = $request->all();
        $data['room'] = $room;
        $data['maxAdult'] = $this->intToArray($room->adult);
        $data['maxChild'] = $this->intToArray($room->child);
        return view('booking.room', $data);
    }

    public function missingMethod ($parameters = Array()) {
        return redirect('/booking');
    }

    private function intToArray($int) {
        $array = array();
        for($i=1; $i<$int+1; $i++) {
            $array[$i] = $i;
        }
        return $array;
    }

    public function getSuccess() {
        return view('booking.success');
    }

    public function sendConfirmCode($user, $password) {
        Mail::send('emails.booking-success', ['password' => $password, 'user' => $user], function($message) use ($user)
        {
            $message->to($user->email, $user->username)->subject('Booking Credentials');
        });
    }

    public function getListBooking() {
        $bookings = booking::all();
        $data['title'] = "List Booking";
        $data['bookings'] = $bookings;
        return view('booking.list-booking', $data);
    }

    public function getMyReservation() {
        $bookings = Auth::user()->booking;
        $data['title'] = "List Booking";
        $data['bookings'] = $bookings;
        return view('booking.my-reservation', $data);
    }

    public function getEditBooking($id=null) {
        if (is_null($id)) return redirect("/admin");
        $booking = Booking::find($id);
        $data['booking'] = $booking;
        $data['title'] = 'Edit Booking';
        return view('booking/edit-booking', $data);
    }

    public function postEditBooking(Request $request) {
        $all = $request->all();


        $booking = Booking::find($all['id']);
        $booking->startDate = date_create_from_format('m/d/Y',$all['checkin'])->getTimestamp();
        $booking->endDate = date_create_from_format('m/d/Y',$all['checkout'])->getTimestamp();
        $booking->save();
        $data['title'] = 'Edit Booking';
        return redirect('/admin');
    }

    public function postDeleteBooking(Request $request) {
        $id = $request->get('id');
        if ($id) {
            $record = booking::find($id);
            $record->delete();
        }
    }
}
