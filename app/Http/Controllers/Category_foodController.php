<?php namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\category_food;
class Category_foodController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{

	}



    public function getCategoryFood() {
        return view ("category_food.categoryfood");
    }


    public function postCategoryFood(Request $request) {
        $data = $request->all();
        $cate_type = new category_food();
        $cate_type->country_name = $data['country_name'];
        $cate_type->type_food=$data['type_food'];
        $cate_type->status=$data['status'];
        $cate_type->save();
        return redirect('/category_food/category-food');
    }

}
