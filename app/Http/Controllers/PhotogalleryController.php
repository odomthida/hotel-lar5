<?php namespace App\Http\Controllers;

use App\Photogallery;

class PhotogalleryController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
    public function __construct()
    {
//        $this->middleware('guest');
    }



    public function getIndex() {
        $data['photos'] = Photogallery::all();
        return view ("gallery.index", $data);
    }


    public function postPhotogallery(Request $request) {
        $data = $request->all();
        $photo = new photogallery();
        $photo->thumbnail = $data['thumbnail'];
        $photo->type=$data['type'];
        $photo->status=$data['status'];
        $photo->save();
        return redirect('/gallery/index');
    }


}
