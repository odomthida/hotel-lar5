<?php namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Services\Registrar;
class AdminController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function getIndex()
	{
		return view('admin.index');
	}


	public function getDashboard() {
		return view('admin.index');
	}

    public function getReservation() {
        $data['title'] = "Reservation";
        return view('admin.reservation', $data);
    }

    public function getListUser() {
        $data['title'] = "List User";
        $data['users'] = User::all();
        return view('admin.listuser', $data);
    }

    public function getAddUser() {
        $data['title'] = "Add User";

        $arrayRole = array();
        foreach (DB::table('user_types')->select('id', 'name')->get() as $usertype) {
            $arrayRole[$usertype->id] =  $usertype->name;
        }
        $data['user_types'] = $arrayRole;
        return view('admin.add-user', $data);
    }

    public function postAddUser(Request $request) {
        $all = $request->all();
        $user = array(
            'firstName' => "",
            'lastName' => "",
            'activation_code' => "",
            'username' => $all['username'],
            'email' => $all['email'],
            'password' => bcrypt($all['password']),
            'active'=>1,
            'user_type_id'=>$all['usertype']
        );
        $registra = new Registrar;
        $validator = $registra->validateAdduser($user);

        if ($validator->fails()) {
            $this->throwValidationException($request, $validator);
        }
        USER::create($user);
        return redirect('/admin/list-user');
    }

    public function postDeleteUser(Request $request) {
        $id = $request->get('id');
        if ($id) {
            $user = User::find($id);
            $user->delete();
        }
    }

	public function getEditUser($id=null) {
		if (is_null($id)) return redirect('admin/listuser');
		$data['title'] = "Edit User";

		$arrayRole = array();
		foreach (DB::table('user_types')->select('id', 'name')->get() as $usertype) {
			$arrayRole[$usertype->id] =  $usertype->name;
		}
		$data['user_types'] = $arrayRole;
		$data['user'] = User::find($id);
		return view('admin.edit-user', $data);
	}

	public function postEditUser($id=null, Request $request) {
		if (is_null($id)) return redirect('admin/listuser');
		$all = $request->all();
		$user = User::find($id);
		$user->username = $all['username'];
		$user->email = $all['email'];
		$user->user_type_id = $all['usertype'];
        $user->active = $all['active'];
		$user->save();
		return redirect('/admin/edit-user/'.$id);
	}
}
