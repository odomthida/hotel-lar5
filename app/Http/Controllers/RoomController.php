<?php namespace App\Http\Controllers;

use App\Photogallery;
use App\Room;
use App\RoomType;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;


class RoomController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');

	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function getIndex() {
        $data['title'] = "Room";
        $rooms = Room::all();
        $data['rooms'] = $rooms;

		return view('room.index', $data);
	}

	public function getCreateRoom() {
		$title  = "Room Create";
		$data['title']= $title;
		return view('room.create-room', $data);
	}

	public function postCreateRoom() {
		$title  = "Post Room Create";
		echo $title;
	}

	public function getCheckRoomAvaileble() {
		echo "Get Check Room";
	}

    public function getAddRoom() {
        $title  = "Room Create";
        $data['title']= $title;

        foreach (DB::table('room_types')->select('id', 'name')->get() as $roomtype) {
            $rooms[$roomtype->id] =  $roomtype->name;
        }
        $facilities=array();
        foreach (DB::table('facilities')->select('id', 'name')->get() as $da) {
            $facilities[$da->id] =  $da->name;
        }
        $data['rooms'] = $rooms;
        $data['facilities'] = $facilities;
        return view('room.add-room', $data);
    }

    public function postAddRoom(Request $request) {
        $all = $request->all();
        $room = new Room();

        $room->name = $all['name'];
        $room->id_room_type = $all['roomtype'];
        $room->price = $all['price'];
        $room->id_facility = $all['facility'];
        $room->description = $all['description'];
        $room->adult = $all['adult']+1;
        $room->child = $all['child']+1;
        $room->save();
        return redirect("/room");
    }

    public function postDeleteRoom(Request $request) {
        $id = $request->get('id');
        if ($id) {
            $record = Room::find($id);
            $record->delete();
        }
    }

    public function getGallery($id = null) {
        if (is_null($id)) return redirect("/room");
        $room = Room::find($id);
        $data['title'] = "Add Gallery";
        $data['room'] = $room;
        return view('room.gallery', $data);
    }

    public function postGallery (Request $request) {
        $id = $request->get("id");
        $photos = $request->file('photos');

        $destinationPath = public_path('upload/'.$id);
        if (! file_exists($destinationPath)) {
            mkdir($destinationPath);
        }

        foreach ($photos as $photo) {
            $gallery = array(
                "thumbnail" => $photo->getClientOriginalName(),
                "path" => $id.'/'.$photo->getClientOriginalName(),
                "id_room" => $id
            );
            Photogallery::create($gallery);
            $photo->move($destinationPath.'/', $photo->getClientOriginalName());
        }
        return redirect('/room/gallery/'.$id);
    }

	public function getEditRoom($id = null) {
		if (is_null($id)) return redirect("/room");

		foreach (DB::table('room_types')->select('id', 'name')->get() as $roomtype) {
			$rooms[$roomtype->id] =  $roomtype->name;
		}
		$facilities=array();
		foreach (DB::table('facilities')->select('id', 'name')->get() as $da) {
			$facilities[$da->id] =  $da->name;
		}
		$data['rooms'] = $rooms;
		$data['facilities'] = $facilities;

		$room = Room::find($id);
		$data['title'] = "Edit Room";
		$data['activeRoom'] = $room;
		return view('room.edit-room', $data);
	}

	public function postEditRoom($id = null, Request $request) {
		if (is_null($id)) return redirect("/room");
		$room = Room::find($id);
		$all = $request->all();
		$room->name = $all['name'];
		$room->id_room_type = $all['roomtype'];
		$room->price = $all['price'];
		$room->id_facility = $all['facility'];
		$room->description = $all['description'];
		$room->adult = $all['adult']+1;
		$room->child = $all['child']+1;
		$room->save();
		return redirect('/room/edit-room/'.$id);
	}

    public function postDeleteGallery(Request $request) {
        $all = $request->all();
        $id = $all['id'];
        if (is_null($id)) return redirect("/room");

        $gallary = Photogallery::find($id);
        if (count($gallary)) $gallary->delete();
    }

    public function getDetail( $id=1 ) {
        $roomType = RoomType::find($id);
            $data['rooms'] = array();
        if (count($roomType)) {
            $data['rooms'] = $roomType->room;
        }
        return view('room.detail', $data);
    }
}
