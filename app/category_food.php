<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class category_food extends Model {

    use SoftDeletes;

    protected $dates = ['deleted_at'];

}
