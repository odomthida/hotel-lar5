<?php namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Room extends Model {

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public function roomtype() {
        return $this->belongsTo('App\RoomType', 'id_room_type');
    }

    public function facility() {
        return $this->belongsTo('App\Facility', 'id_facility');
    }

    public function photos() {
        return $this->hasMany('App\Photogallery','id_room');
    }

    public function booking() {
        return $this->hasMany('App\booking','id_room');
    }


}
