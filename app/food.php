<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class food extends Model {

    use SoftDeletes;

    protected $dates = ['deleted_at'];

}
