<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('rooms', function(Blueprint $table)
		{
            $table->increments('id');
            $table->string("name");
			$table->integer('id_facility')->unsigned();
			$table->integer('id_room_type')->unsigned();
            $table->float('price')->default(0);
            $table->string('adult')->default(1);
            $table->string('child')->default(1);
			$table->text('description')->nullable();

			$table->foreign('id_facility')
				->references('id')->on('facilities');
            $table->foreign('id_room_type')
				->references('id')->on('room_types');

			$table->timestamps();
            $table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('rooms');
	}

}
