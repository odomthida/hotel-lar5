<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Facility;

class CreateFacilitiesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('facilities', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name', 300);
            $table->string('icon')->nullable();
            $table->text("description");
            $table->boolean('status')->default(1);
            $table->timestamps();
            $table->softDeletes();
        });
        $this->dataCreate();
    }


    public function dataCreate() {
        $datas = array();
        $items = array(
            "package 1" => "
                    <ul>
                    <li>Bed: 1</li>
                    <li>Size : 30sqm</li>
                    </ul>
                ",
            "package 2" => "
                    <ul>
                    <li>Bed: 1</li>
                    <li>Size : 40sqm</li>
                    </ul>
                    ",
            "package 3" => "
                    <ul>
                    <li>Bed: 2</li>
                    <li>Size : 40sqm</li>
                    </ul>
            ",
            "package 4" => "
                    <ul>
                    <li>Bed: 2</li>
                    <li>Size : 50sqm</li>
                    </ul>
            ",
            "package 5" => "
                    <ul>
                    <li>Bed: 2</li>
                    <li>Size : 60sqm</li>
                    </ul>
            ",
        );
        foreach ($items as $item => $value){
            $datas[] = array(
                'name' => $item,
                'description' => $value,
            );
        }

        foreach ($datas as $data) {
            facility::create($data);
        }

    }

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('facilities');
	}

}
