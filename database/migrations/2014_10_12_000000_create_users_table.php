<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\User;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('firstName');
			$table->string('lastName');
			$table->string('email')->unique();
			$table->string('username')->unique();
			$table->string('image_data')->nullable();
			$table->text('thumbnail')->nullable();
			$table->text('address')->nullable();
			$table->string('phone')->nullable();
			$table->string('gender')->nullable();
			$table->boolean('active')->default(0);
			$table->date('dob')->nullable();
			$table->string('password', 60);
			$table->string('activation_code');
            $table->integer("user_type_id")->unsigned();

			$table->rememberToken();
			$table->timestamps();
            $table->softDeletes();

            $table->foreign('user_type_id')
                ->references('id')->on('user_types');
		});
        $this->addDefaultData();
	}


    public function addDefaultData() {
        $users = array(
	    array (
            "firstName" => "Asecro",
            "lastName" => "Hotel",
            "username" => "asecro_hotel",
            "email" => "admin@asecro.com",
            "password" => "admin123",
		    "activation_code"=>"",
		    "active" => 1,
            "user_type_id" => 1,
        ), array (
		        "firstName" => "Thida",
		        "lastName" => "Heng",
		        "username" => "hengthida",
		        "email" => "thida@asecro.com",
		        "password" => "thida123",
		        "activation_code"=>"",
		        "active" => 1,
                "user_type_id" => 1,
        ), array (
		        "firstName" => "Hem",
		        "lastName" => "Sinat",
		        "username" => "heumsinat",
		        "email" => "sinat@asecro.com",
		        "password" => "sinat123",
		        "activation_code"=>"",
		        "active" => 1,
                "user_type_id" => 1,
        ));

        foreach ($users as $user) {
            $newUser = new \App\User();
	        $newUser->firstName = $user['firstName'];
	        $newUser->lastName = $user['lastName'];
	        $newUser->username = $user['username'];
	        $newUser->email = $user['email'];
            $newUser->password = bcrypt($user['password']);
	        $newUser->active=1;
	        $newUser->user_type_id= $user['user_type_id'];
	        $newUser->activation_code="";
            $newUser->save();
        }
    }
	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
