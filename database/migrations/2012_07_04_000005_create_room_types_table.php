<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomTypesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('room_types', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->timestamps();
            $table->softDeletes();
		});
        $this->sample();
	}

    public function sample () {
        $datas= array(
            array("name"=> "Single Room"),
            array("name"=> "Couple Room"),
            array("name"=> "Double Room")
        );

        foreach($datas as $data){
            \App\RoomType::create($data);
        }
    }

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('room_types');
	}

}
