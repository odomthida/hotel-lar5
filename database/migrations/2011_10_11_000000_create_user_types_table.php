<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\UserType;

class CreateUserTypesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_types', function(Blueprint $table)
        {

			$table->increments('id');
			$table->string('name');
			$table->timestamps();
            $table->softDeletes();
		});
        $this->addDefaultData();
        }


public function addDefaultData() {
    $usertypes = array(
        array (
            "name" => "Admin",
        ), array (
            "name"=>"Staff",
        ), array (
            "name"=>"normal",
        ));

    foreach ($usertypes as $type) {
        UserType::create($type);
    }
}
	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_types');
	}

}
