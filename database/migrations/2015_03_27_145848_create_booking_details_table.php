<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingDetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('booking_details', function(Blueprint $table)
		{
            $table->increments('id');
            $table->integer('id_booking');
            $table->integer('id_product');
            $table->date('checkIn');
            $table->date('checkOut');
            $table->float('price');
            $table->string('customer_name', 200);
            $table->string('address', 200);
            $table->integer('phonenumber');
            $table->string('email', 200);
            $table->string('special_request', 500);
            $table->date('cancellation_policy');
            $table->string('status', 300);
            $table->integer('create_by');
            $table->integer('modify_by');
            $table->timestamps();
            $table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('booking_details');
	}

}
