<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFoodsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('foods', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('id_category')->unsigned();
			$table->integer('id_gallery')->unsigned();
			$table->string('food_name', 300);
			$table->float('price')->default(0);
			$table->text('description')->nullable();
			$table->tinyInteger('status')->default(1);

			$table->foreign('id_category')
				->references('id')->on('category_foods');
			$table->foreign('id_gallery')
				->references('id')->on('photogalleries');
			$table->timestamps();
            $table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('foods');
	}

}
