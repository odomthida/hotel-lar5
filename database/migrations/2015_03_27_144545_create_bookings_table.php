<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('bookings', function(Blueprint $table)
		{
            $table->increments('id');
            $table->string('credit_card_number')->nullable();
            $table->integer('id_user')->unsigned();
            $table->integer('id_room')->unsigned();
            $table->integer('endDate')->unsigned();
            $table->integer('startDate')->unsigned();

            $table->foreign('id_user')
                ->references('id')->on('users');

            $table->foreign('id_room')
                ->references('id')->on('rooms');


            $table->timestamps();
            $table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('bookings');
	}

}
